
# <city-input-tag><city-input-tag\>

Create city tag list

## Install the Polymer-CLI
* First, make sure you have the [Polymer CLI](https://www.npmjs.com/package/polymer-cli) installed. 
* Then, run :

```
$ bower install
```

## Viewing Your Element

```
$ polymer serve
```
* Go to `http://localhost:8081/`

## Running Tests

```
$ polymer test
```

Your application is already set up to be tested via [web-component-tester](https://github.com/Polymer/web-component-tester). Run `polymer test` to run your application's test suite locally.